# Getting Started

Acessar Swagger (http://localhost:8080/swagger-ui/index.html)

Test

POST (http://localhost:8080/api/v1/livros)
`
	{
	    "titulo": "Meu Test",
	    "idioma": "Português",
	    "paginas": 10
	}
`

PUT (http://localhost:8080/api/v1/livros/1)
`
	{
	    "titulo": "Meu Primeiro Update",
	    "idioma": "Português",
	    "paginas": 200
	}
`

GET Um (http://localhost:8080/api/v1/livros/1)
`
	{
	    "codigo": 1,
	    "titulo": "Meu Primeiro Update",
	    "idioma": "Português",
	    "paginas": 200,
	    "ativo": true,
	    "criadoEm": "2021-08-07T00:39:23.771+0000",
	    "atualizadoEm": "2021-08-07T00:43:43.178+0000"
	}
`

Get Todos (http://localhost:8080/api/v1/livros)
`
{
    "content": [
        {
            "codigo": 1,
            "titulo": "Meu Primeiro Update",
            "idioma": "Português",
            "paginas": 200,
            "ativo": true,
            "criadoEm": "2021-08-07T00:39:23.771+0000",
            "atualizadoEm": "2021-08-07T00:43:43.178+0000"
        },
        {
            "codigo": 2,
            "titulo": "Meu Outro Test",
            "idioma": "Portugues",
            "paginas": 10,
            "ativo": true,
            "criadoEm": "2021-08-07T00:44:02.612+0000",
            "atualizadoEm": "2021-08-07T00:44:02.611+0000"
        }
    ],
    "pageable": {
        "sort": {
            "sorted": false,
            "unsorted": true,
            "empty": true
        },
        "offset": 0,
        "pageNumber": 0,
        "pageSize": 20,
        "unpaged": false,
        "paged": true
    },
    "last": true,
    "totalElements": 2,
    "totalPages": 1,
    "size": 20,
    "number": 0,
    "sort": {
        "sorted": false,
        "unsorted": true,
        "empty": true
    },
    "numberOfElements": 2,
    "first": true,
    "empty": false
}
`

Delete (http://localhost:8080/api/v1/livros/1)
`
	Not Content
`
