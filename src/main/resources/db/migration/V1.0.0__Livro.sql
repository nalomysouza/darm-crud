CREATE TABLE public.livro (
   	codigo bigserial NOT NULL,
   	titulo character varying (255) NOT NULL,
   	idioma character varying (20),
   	paginas integer,
   	ativo boolean NOT NULL DEFAULT true,
   	atualizado_em timestamp with time zone,
	criado_em timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
   	CONSTRAINT livro_pkey PRIMARY KEY (codigo)
);

