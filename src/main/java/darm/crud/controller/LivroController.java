package darm.crud.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import darm.crud.LivroService;
import darm.crud.config.exception.NegocioException;
import darm.crud.model.Livro;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/livros")
public class LivroController {

	@Autowired
	private LivroService service;

	@Operation(summary = "Busca o livro pelo {codigo}")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Livro encontrado pelo {codigo}", content = {
					@Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = ResponseEntity.class))) }),
			@ApiResponse(responseCode = "204", description = "Nenhum livro encontrado com {codigo}", content = @Content) })
	@GetMapping("/{codigo}")
	public ResponseEntity<Livro> read(@PathVariable(value = "codigo") long codigo) {
		Optional<Livro> livro = this.service.findById(codigo);
		if (livro.isPresent()) {
			return ResponseEntity.status(HttpStatus.OK).body(livro.get());
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Operation(summary = "Buscar todos")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "todos os livros", content = {
			@Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = ResponseEntity.class))) }),
			@ApiResponse(responseCode = "204", description = "Nenhum objeto encontrado", content = @Content) })
	@GetMapping("/all")
	public List<Livro> all() {
		return this.service.all();
	}

	@Operation(summary = "Buscar todos com paginação")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "todos os livros paginados", content = {
			@Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = ResponseEntity.class))) }),
			@ApiResponse(responseCode = "204", description = "Nenhum livro encontrado", content = @Content) })
	@GetMapping
	public Page<Livro> all(Pageable page) {
		return this.service.all(page);
	}

	@Operation(summary = "Inserindo um livro")
	@ApiResponses(value = { @ApiResponse(responseCode = "201", description = "livro criado", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }),
			@ApiResponse(responseCode = "404", description = "Bad request", content = @Content) })
	@PostMapping
	public ResponseEntity<Livro> create(@Valid @RequestBody Livro entity) {
		Livro created;
		try {
			created = this.service.createOrUpdate(entity);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(created);
	}

	@Operation(summary = "Atualizando um livro")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Livro atualizado", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }),
			@ApiResponse(responseCode = "404", description = "Nenhum objeto encontrado com este {id}", content = @Content) })
	@PutMapping(value = "/{codigo}")
	public Livro update(@PathVariable(value = "codigo") Long codigo, @Valid @RequestBody Livro entity) {
		return this.service.findById(codigo).map(o -> {
			entity.setCodigo(codigo);
			entity.setCriadoEm(new Date());
			return this.service.createOrUpdate(entity);
		}).orElseThrow(() -> new NegocioException());
	}

	@Operation(summary = "Deleta o objeto")
	@ApiResponses(value = { @ApiResponse(responseCode = "204", description = "objeto deletado"),
			@ApiResponse(responseCode = "204", description = "Nenhum objeto encontrado", content = @Content),
			@ApiResponse(responseCode = "404", description = "Nenhum recurso encontrado", content = @Content) })
	@DeleteMapping(value = "/{codigo}")
	public ResponseEntity<Void> delete(@PathVariable(value = "codigo") long codigo) {
		try {
			this.service.delete(codigo);
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.noContent().build();
	}
}
