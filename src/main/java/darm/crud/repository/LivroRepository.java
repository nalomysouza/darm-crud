package darm.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import darm.crud.model.Livro;

/**
 * repository {@link Livro}
 * 
 * @author Nalomy Souza
 *
 */
@Repository
public interface LivroRepository extends JpaRepository<Livro, Long> {

}
