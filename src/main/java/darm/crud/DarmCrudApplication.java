package darm.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class DarmCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(DarmCrudApplication.class, args);
	}

}
