package darm.crud;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import darm.crud.model.Livro;
import darm.crud.repository.LivroRepository;

@Service
public class LivroService {

	@Autowired
	private LivroRepository repository;

	@Transactional(readOnly = true)
	public Optional<Livro> findById(Long id) {
		return this.repository.findById(id);
	}

	@Transactional(readOnly = true)
	public List<Livro> all() {
		return repository.findAll();
	}

	@Transactional(readOnly = true)
	public Page<Livro> all(Pageable pageable) {
		return this.repository.findAll(pageable);
	}

	@Transactional
	public Livro createOrUpdate(Livro livro) {
		return this.repository.save(livro);
	}

	@Transactional(readOnly = true)
	public Long count() {
		return this.repository.count();
	}

	@Transactional
	public void delete(Long id) {
		this.repository.deleteById(id);
	}
}
