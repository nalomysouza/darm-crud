package darm.crud.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createIn", "updateIn" }, allowGetters = true)
public class Livro {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@NotEmpty
	@Column(name = "titulo")
	private String titulo;

	@Column(name = "idioma")
	private String idioma;

	@Column(name = "paginas")
	private int paginas;

	@Column(name = "ativo")
	private boolean ativo = true;

	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	@Column(name = "criado_em", nullable = false, insertable = false, updatable = false)
	private Date criadoEm;

	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	@Column(name = "atualizado_em")
	private Date atualizadoEm;

}
